#!/bin/bash

list_courses(){
	set -x
	echo "debug at l.16"
	BASE_DIR=$(pwd)
#	echo "debug at l.18"	
	for folder in "$@"; do
		echo "$folder"
		cd "$folder" 
		ls
		if [[ -f info.yml && (! -f index.html) ]] ; then
			create_variables info.yml
			case  $type in 
			"menu")
				echo "MENU: $title"
				cp -vf "$BASE_DIR/includes/html/index-menu.html" index.html
				sed -i "s|<title>Default</title>|<title>$title</title>|" index.html
				sed -i "s|<h1>Default</h1>|<h1>$title</h1>|" index.html
				for subfolder in *; do
					if [[ -f "$subfolder/info.yml" ]] ; then
						obj=()
						create_variables "$subfolder/info.yml"
						echo "title of subcard : $title in $subfolder"
						echo "                       <div class=\"card\" data-link=\"/$folder/$subfolder\">" >> index.html
						echo "                            <div class=\"card-body\">" >> index.html
						echo "                                <h5 class=\"card-title\">$title<i class=\"material-icons\">arrow_right</i></h5>" >> index.html
						echo "                                <p>$desc</p>" >> index.html
						if [ "$type" != "menu" ] ; then
							echo "                                <ul>" >> index.html
							for j in "${obj[@]}"; do
								echo "                                    <li>$j</li>" >> index.html
							done
							echo "                                </ul>" >> index.html
						fi
						echo "                            </div>" >> index.html
						echo "                        </div>" >> index.html
						obj=()
					fi
				done
				cat << EOF >> index.html
					</div>
				</div>
            </div>
        </main>
        <script src="/includes/js/select-course.js"></script>
    </body>
</html>
EOF
				;;
			"slide")
				echo "SLIDE : $title"
				cp -vf "$BASE_DIR/includes/html/index-reveal.html" index.html
				sed -i "s|<title>Default</title>|<title>$title</title>|" index.html
				;; 
			"simple")
				echo "PAGE: $title"
				cp -vf README.md $BASE_DIR/includes/lib/markdown-to-html/
				node $BASE_DIR/includes/lib/markdown-to-html/convert.js MarkdownToHTML
				cp -fv $BASE_DIR/includes/lib/markdown-to-html/index.html .
				;;
			esac
		else
			echo "no yaml or existing index in $folder"
		fi
		cd "$BASE_DIR"
	done
}

set -x
mkdir -p public
cp -Rf tmp_includes/includes public/
cp -Rf courses public/
cd public
source includes/sh/yaml.sh
export -f parse_yaml
export -f create_variables
export -f list_courses
find -- courses -type d -print0 |xargs -0 -I {} bash -c 'list_courses {}' \;
ln -fs courses/index.html
pkill python3 || true  
python3 -m http.server&  #port is 8000
