#!/bin/bash
set -x

# Install dependencies in local build
local_deps(){
	curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
	apt update
	apt -y install nodejs build-essential git curl wget python3 gcc g++ make
}
export -f local_deps
local_deps || true
	

build_reveal(){
	rm -fr reveal
	git clone https://github.com/hakimel/reveal.js.git
	mv reveal.js reveal
	cd reveal
	rm -fr .git*
	npm install
	cd ..
	curl -O https://cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.min.js
	curl  https://raw.githubusercontent.com/eligrey/classList.js/master/classList.min.js > classList.js
}

build_md2html(){
	rm -fr markdown-to-html
	git clone https://github.com/cwjohan/markdown-to-html.git
	cp -vf ../css/style-md2html.css markdown-to-html/style.css
	cd markdown-to-html
	rm -fr .git*
	npm install
	cd ..
}

mkdir -p tmp_includes
cp -rf includes tmp_includes/
cd tmp_includes/includes/lib
build_reveal
build_md2html
cd ../../../
