# Bienvenue
au garage numérique<!-- .element: class="fragment" -->

Note: Penser à remercier les partenaires



---



Le premier cours  

D'info et de maths
<!-- .element: class="fragment fade-left" data-fragment-index="1"-->
`$$ J(\theta_0,\theta_1) = \sum_{i=0} $$`
<!-- .element: class="fragment fade-up" data-fragment-index="2"--> 



***



Avec la coloration syntaxique:  
```
<!-- Ceci est un commentaire ! -->
<!DOCTYPE html> <!-- sert à indiquer que l'on utilise HTML 5 -->
<html> <!-- balise racine présente dans chaque page web -->
    <head> <!-- les méta-données de la page -->
        <meta charset="UTF-8">
```  
```
apt install nfs-kernel-server nfs-common isc-dhcp-server tftpd-hpa \
iptables-persistent apache2 apt-mirror 
cp -Rfv  pxe-conf/tftp/* /srv/tftp/
cd pxe-conf/server-conf 
```
<!-- .element: class="fragment fade-left" -->
```
minetest.register_node("custom_nodes:invisible", {
	description = "Invisible wall",
	tiles = {
		"default_invisible.png", "default_invisible.png",
		"default_invisible.png", "default_invisible.png",
		"default_invisible.png", "default_invisible.png"
	},
	--paramtype2 = "facedir",
	groups = {cracky=2, level=4},
	use_texture_alpha=true,
	drawtype = "torchlike",
})
```   
<!-- .element: class="fragment fade-right" -->



***


![text shown if image not found](Walking_baby_tux.gif "Walking tux")
